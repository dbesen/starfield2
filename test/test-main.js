// The content of this file was taken from http://karma-runner.github.io/0.10/plus/requirejs.html.
var tests = [];
for (var file in window.__karma__.files) {
    if (window.__karma__.files.hasOwnProperty(file)) {
        if (/^\/base\/test\/.*Spec\.js$/.test(file)) {
            tests.push(file);
        }
    }
}

requirejs.config({
    // Karma serves files from '/base'
    baseUrl: '/base/src',

    paths: {
        'test': '/base/test',
        'jquery': '/base/node_modules/jquery/dist/jquery',
        'jasmine-jquery': '/base/node_modules/jasmine-jquery/lib/jasmine-jquery'
    },

    shim: {
        'jasmine-jquery': {
            deps: ['jquery']
        }
    },

    // ask Require.js to load these files (all our tests)
    deps: tests,

    // start test run, once Require.js is done
    callback: window.__karma__.start
});
