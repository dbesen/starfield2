define(['renderer', 'test/testBase'], function(renderer) {
    describe("renderer", function() {

        it("should have a context", function() {
            expect(renderer.getContext()).not.toBeNull();
        });

        it("should be able to render a pixel", function() {
            expect(renderer.pixelAt(10,20)).toBeColor(0, 0, 0, 0);
            renderer.renderPixel(10, 20, 1, 2, 3);
            expect(renderer.pixelAt(10,20)).toBeColor(1, 2, 3, 255);
        });

        it("should be able to render a pixel again", function() {
            // This test failed even though it's the same as the one above.
            // It was because I was storing too much state.
            expect(renderer.pixelAt(10,20)).toBeColor(0, 0, 0, 0);
            renderer.renderPixel(10, 20, 1, 2, 3);
            expect(renderer.pixelAt(10,20)).toBeColor(1, 2, 3, 255);
        });

        it("should be able to overwrite a pixel", function() {
            renderer.renderPixel(10, 20, 1, 2, 3);
            renderer.renderPixel(10, 20, 4, 5, 6);
            expect(renderer.pixelAt(10,20)).toBeColor(4, 5, 6, 255);
        });

        it("should be able to render a circle", function() {
            renderer.renderCircle(20, 30, 10, "rgb(2,3,4)");
            // Center
            expect(renderer.pixelAt(20,30)).toBeColor(2, 3, 4, 255);
            // Bottom
            expect(renderer.pixelAt(20,38)).toBeColor(2, 3, 4, 255);
            // The pixel at 20,39 can depend on browser-specific antialiasing
            expect(renderer.pixelAt(20,40)).toBeColor(0, 0, 0, 0);
            // Right
            expect(renderer.pixelAt(28,30)).toBeColor(2, 3, 4, 255);
            expect(renderer.pixelAt(30,30)).toBeColor(0, 0, 0, 0);
            // Left
            expect(renderer.pixelAt(11,30)).toBeColor(2, 3, 4, 255);
            expect(renderer.pixelAt(9,30)).toBeColor(0, 0, 0, 0);
            // Top
            expect(renderer.pixelAt(20,21)).toBeColor(2, 3, 4, 255);
            expect(renderer.pixelAt(20,19)).toBeColor(0, 0, 0, 0);
        });

        it("should have a renderText function", function() {
            renderer.renderText(1, 2, "text");
        });
    });
});
