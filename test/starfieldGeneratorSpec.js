define(['starfieldGenerator', 'test/testBase'], function(generator) {
    describe("starfieldGenerator", function() {
        // The thing that defines correctness with a random starfield is not the location of any individual star.  It's random!
        // The positions of the stars generated shouldn't be too narrow or too wide.
        // We shouldn't render too many stars in a given field.
        // We shouldn't render too few stars in a given field.
        // The generated starfield should change if we change the seed.
        // The number of stars we generate should scale with the size of the area we're generating in.
        // The stars should be sorted by y position to facilitate speed when rendering.
        // Advanced: the stars shouldn't all be in a line or grid.
        // Advanced: the density of stars should change with location (ala perlin)

        it('should have a generate function', function() {
            generator.generate();
        });

        it('should create at least two stars', function() {
            stars = generator.generate();
            expect(stars.length).toBeGreaterThan(1);
        });

        it('should return something with a position', function() {
            stars = generator.generate();
            first = stars[0];
            expect(first.position).toBeDefined();
            expect(first.position).not.toBeNull();
        });

        it('should vary positions of stars', function() {
            stars = generator.generate();
            first = stars[0];
            second = stars[1];

            expect(first.position).not.toEqual(second.position);
        });

        it('should vary between calls', function() {
            first = generator.generate()[0];
            second = generator.generate()[0];

            expect(first.position).not.toEqual(second.position);
        });

        it('should let you specify star density', function() {
            stars = generator.generate(100, 100, 1)
            expect(stars.length).toEqual(1);
        });

        it('should let you specify a different star density', function() {
            stars = generator.generate(100, 100, 10)
            expect(stars.length).toEqual(10);
        });

        it('should be density, not just number of stars', function() {
            stars = generator.generate(100, 200, 10)
            expect(stars.length).toEqual(20);
        });

    });
});
