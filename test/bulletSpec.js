define(['starfield2', 'bullet', 'renderer', 'test/testBase'], function(starfield2, bullet, renderer) {
    describe("Bullet", function() {

        it("should be able to draw", function() {
            b = new bullet();
            b.x = 30;
            b.y = 40;
            starfield2.registerComponent(b);
            starfield2.nextFrame();
            expect(renderer.pixelAt(30,40)).toBeColor(0,255,255,255);
            expect(renderer.pixelAt(34,40)).toBeColor(0,0,255,255);
        });
    });
});
