define(['starfield2', 'renderer', 'test/testBase'], function(starfield2, renderer) {
    describe("Starfield 2", function() {

        it("should have a function to render a frame", function() {
            starfield2.nextFrame();
        });

        it("should call draw on registered components", function() {
            var called = false;
            component = {
                draw: function() {
                    called = true;
                }
            }
            starfield2.registerComponent(component);
            expect(called).toBe(false);
            starfield2.nextFrame();
            expect(called).toBe(true);
        });

        it("should call update on registered components", function() {
            var called = false;
            component = {
                update: function() {
                    called = true;
                }
            };
            starfield2.registerComponent(component);
            expect(called).toBe(false);
            starfield2.nextFrame();
            expect(called).toBe(true);
        });

        it("should call draw before update", function() {
            drawCalled = false;
            component = {
                draw: function() {
                    drawCalled = true;
                },
                update: function() {
                    expect(drawCalled).toBe(true);
                }
            };
            starfield2.registerComponent(component);
            starfield2.nextFrame();
        });

        it("should track time between frames", function() {
            spy = spyOn(window.performance, "now");

            elapsed = 0;
            component = {
                update: function(e) {
                    elapsed = e;
                }
            };

            starfield2.registerComponent(component);

            // After registering, update should not have been called.
            expect(elapsed).toBe(0);

            spy.andReturn(10);
            starfield2.nextFrame();

            // For the first frame, we don't have a time between frames.
            expect(elapsed).toBe(0);

            spy.andReturn(30);
            starfield2.nextFrame();

            // After the second frame, elapsed should be the time since the previous frame.
            expect(elapsed).toBe(20);
        });

        it("should blank between frames", function() {
            renderer.renderCircle(10, 10, 10, "#f00");
            expect(renderer.pixelAt(10,10)).toBeColor(255, 0, 0, 255);
            starfield2.nextFrame();
            expect(renderer.pixelAt(10,10)).toBeColor(0, 0, 0, 255);
        });

        it("should have a black background", function() {
            starfield2.nextFrame();
            expect(renderer.pixelAt(10,10)).toBeColor(0, 0, 0, 255);
        });
    });
});
