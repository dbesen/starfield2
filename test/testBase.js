define(['starfield2', 'jasmine-jquery'], function(starfield2) {
    beforeEach(function() {
        // Simulate starting from scratch by calling init each time
        starfield2.init();

        jasmine.getFixtures().fixturesPath = '/base/src/';
        loadFixtures("index.html");

        this.addMatchers({
            toBeColor: function(r, g, b, a) {
                var actual = this.actual.data;

                this.message = function() {
                    // Firefox doesn't tostr the image data, so we need this to show it
                    return "Expected color "
                        + actual[0] + "," + actual[1] + "," + actual[2] + "," + actual[3]
                        + " to equal "
                        + r + "," + g + "," + b + "," + a;
                };
                if(actual[0] != r) return false;
                if(actual[1] != g) return false;
                if(actual[2] != b) return false;
                if(actual[3] != a) return false;
                return true;
            }
        });

    });
});
