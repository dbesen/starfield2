define(['starfield2', 'fpsComponent', 'test/testBase'], function(starfield2, fpsComponent) {
    describe("FPS component", function() {

        it("should calculate frames per second", function() {
            spy = spyOn(window.performance, "now");

            starfield2.registerComponent(fpsComponent);

            expect(fpsComponent.fps).toBe(0);

            spy.andReturn(500);
            starfield2.nextFrame();

            expect(fpsComponent.fps).toBe(0);

            spy.andReturn(2500);
            starfield2.nextFrame();

            expect(fpsComponent.fps).toBe(.5);

        });
    });
});
