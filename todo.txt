* Prioritize this backlog

Make the starfield background move forward with time
The star object position shouldn't change each time the method is called
Vary radius and color of stars
(performance) The getContext and clear methods, and part of renderPixel, in renderer.js should only happen once per page load
Generated stars should be sorted by vertical position, to facilitate fast slicing for deciding which to render
The star distribution should not be uniform.  Some should be grouped closely together (more like the real world)
The background should give the illusion of depth by having different stars move at different speeds.
Stars should twinkle. (vary their size/color over time)
Render the player's ship
Make the player's ship move around with arrow keys or wsad
Make the player travel faster the higher on the screen he/she is
Make the player shoot
Make the player's shot stronger the further he/she gets
Render an enemy
Make the enemy shoot
Make the game's playable area aspect ratio fixed (when you resize the window, aspect ratio shouldn't change)
Make some complicated bullet patterns
Add some new enemies
Add different levels
Add a way to jump between levels (in case a player plays a few levels, then goes to another computer)
Jitter graph
 - Use it to choose between window.performance.now and Date.now.  Also for performance profiling.
Automated tests that make-dist.sh works right (isn't missing dependencies, etc)
Minification of js, including dependencies (like require.js)
How do I do performance profiling?
display test for the renderText function (it's untested right now)
requestAnimationFrame doesn't call if the window is on a background tab?  Does that cause problems?
Phone interface
