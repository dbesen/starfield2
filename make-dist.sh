#!/bin/bash

set -e

cd "$( dirname "${BASH_SOURCE[0]}" )"

if [ -e dist ]; then
    rm -rf dist/
fi

./install-npm-requires.sh

mkdir dist
cp src/* dist
cp node_modules/requirejs/require.js dist
