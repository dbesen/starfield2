define(['starfield2', 'renderer'], function(starfield2, renderer) {
    return {
        fps: 0,

        draw: function() {
            renderer.renderText(0, 0, this.fps);
        },

        update: function(elapsed) {
            if(elapsed > 0)
                this.fps = 1000 / elapsed;
        }
    };
});
