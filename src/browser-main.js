require(["starfield2", "fpsComponent", "starfieldGeneratorComponent"], function(starfield2, fpsComponent, starfieldGeneratorComponent) {
    callback = function() {
        starfield2.nextFrame();
        window.requestAnimationFrame(callback);
    };
    starfield2.registerComponent(fpsComponent);
    starfield2.registerComponent(starfieldGeneratorComponent);
    window.requestAnimationFrame(callback);
});
