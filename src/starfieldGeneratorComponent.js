define(['starfield2', 'renderer', 'starfieldGenerator'], function(starfield2, renderer, generator) {
    field = generator.generate();
    return {
        draw: function() {
            field.map(function(item) {
                item.draw();
            });
        },

        update: function(elapsed) {
        }
    };
});
