define(['renderer'], function(renderer) {
    return function() {
        var x, y;
        this.draw = function() {
            ctx = renderer.getContext();
            ctx.beginPath();
            ctx.arc(this.x, this.y, 5, 0, Math.PI * 2, false);
            ctx.fillStyle = "#00FFFF";
            ctx.fill();
            ctx.lineWidth = 2;
            ctx.strokeStyle = "#0000FF";
            ctx.stroke();
        };
    };
});
