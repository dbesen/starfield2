define(['renderer'], function(renderer) {
    ret = {
        init: function() {
            this.components = [];
            this.lastFrameTime = null;
        },

        registerComponent: function(component) {
            this.components.push(component);
        },

        drawComponents: function() {
            this.components.forEach(function(component) {
                if(component.draw) component.draw();
            });
        },

        updateComponents: function(elapsed) {
            this.components.forEach(function(component) {
                if(component.update) component.update(elapsed);
            });
        },

        getElapsed: function() {
            now = window.performance.now();
            if(!this.lastFrameTime) {
                elapsed = 0;
            } else {
                elapsed = now - this.lastFrameTime;
            }
            this.lastFrameTime = now;
            return elapsed;
        },

        nextFrame: function() {
            // We call draw before update in order to render as near to vertical blank as we can.
            renderer.clear();
            this.drawComponents();
            this.updateComponents(this.getElapsed());
        }
    };
    ret.init();
    return ret;
});
