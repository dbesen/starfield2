define(['renderer'], function(renderer) {
    return function(width, height) {
        this.position = {
            x: Math.random() * width,
            y: Math.random() * height
        },
        this.draw = function() {
            renderer.renderCircle(this.position.x, this.position.y, 1, "#FFF");
        }
    };
});
