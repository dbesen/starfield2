define(['renderer', 'star'], function(renderer, star) {
    return {
        generate: function(width, height, density) {
            if(!width) width = 900;
            if(!height) height = 900;
            if(!density) density = 2;
            // density = 10 means 10 stars per 100*100 block
            // 900*900 = 18 100*100 blocks, so num stars would be 180
            num_stars = (width / 100) * (height / 100) * density;
            ret = [];
            for(i=0;i<num_stars;i++) {
                ret.push(new star(width, height));
            }
            return ret;
        }
    };
});
