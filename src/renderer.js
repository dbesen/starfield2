define([], function() {
    return {
        getContext: function() {
            return window.document.getElementById('myCanvas').getContext("2d");
        },

        clear: function() {
            canvas = window.document.getElementById('myCanvas');

            ctx = this.getContext();
            ctx.fillStyle="#000";
            ctx.fillRect(0, 0, canvas.width, canvas.height);
        },

        pixelAt: function(x, y) {
            return this.getContext().getImageData(x, y, 1, 1);
        },

        renderPixel: function(x, y, r, g, b, a) {
            // http://stackoverflow.com/questions/4899799/whats-the-best-way-to-set-a-single-pixel-in-an-html5-canvas
            ctx = this.getContext();
            id = ctx.createImageData(1,1);
            d = id.data;

            if(!a) a = 255;

            d[0] = r;
            d[1] = g;
            d[2] = b;
            d[3] = a;
            ctx.putImageData( id, x, y );
        },

        renderCircle: function(x, y, radius, style) {
            ctx = this.getContext();
            ctx.beginPath();
            ctx.arc(x, y, radius, 0, Math.PI * 2, false);
            ctx.fillStyle = style;
            ctx.fill();
        },

        renderText: function(x, y, text, font) {
            ctx = this.getContext();
            ctx.textBaseline = "top";
            if(font) ctx.font = font;
            ctx.fillStyle = "#fff";
            ctx.fillText(text, x, y);
        }
    };
});
