// Karma configuration
// Generated on Sat Feb 01 2014 11:35:46 GMT-0700 (MST)

module.exports = function(config) {
  config.set({

    // base path, that will be used to resolve files and exclude
    basePath: '',


    // frameworks to use
    frameworks: ['jasmine', 'requirejs'],

    // This is needed so fixtures can load require.js.
    // TODO: does this slow down the tests much?  Is there a more Karma-ish way to do this?
    proxies: {
      '/require.js': 'http://localhost:9876/base/node_modules/requirejs/require.js'
    },


    // list of files / patterns to load in the browser
    files: [
      'test/test-main.js',
      {pattern: 'src/**/*.js', included: false},
      {pattern: 'test/**/*.js', included: false},
      {pattern: 'node_modules/**/*.js', included: false},
      {pattern: 'src/**/*.html', included: false}
    ],


    // list of files to exclude
    exclude: [
    ],


    // We don't want to load the html2js plugin (loaded by default)
    preprocessors: {
        '**/*.html': [],
    },


    // test results reporter to use
    // possible values: 'dots', 'progress', 'junit', 'growl', 'coverage'
    reporters: ['progress'],


    // web server port
    port: 9876,


    // enable / disable colors in the output (reporters and logs)
    colors: true,


    // level of logging
    // possible values: config.LOG_DISABLE || config.LOG_ERROR || config.LOG_WARN || config.LOG_INFO || config.LOG_DEBUG
    logLevel: config.LOG_INFO,


    // enable / disable watching file and executing tests whenever any file changes
    autoWatch: true,


    // Start these browsers, currently available:
    // - Chrome
    // - ChromeCanary
    // - Firefox
    // - Opera (has to be installed with `npm install karma-opera-launcher`)
    // - Safari (only Mac; has to be installed with `npm install karma-safari-launcher`)
    // - PhantomJS
    // - IE (only Windows; has to be installed with `npm install karma-ie-launcher`)
    browsers: [],


    // If browser does not capture in given timeout [ms], kill it
    captureTimeout: 60000,


    // Continuous Integration mode
    // if true, it capture browsers, run tests and exit
    singleRun: false
  });
};
