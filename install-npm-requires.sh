#!/bin/bash

set -e

if [ -e 'node_modules' ]; then
    echo "Reusing existing node_modules"
    exit 0
fi

# Needed for older versions of npm
npm config set registry http://registry.npmjs.org/

for i in `cat npm-requires.txt`; do
    echo Installing $i...
    npm install $i
done
